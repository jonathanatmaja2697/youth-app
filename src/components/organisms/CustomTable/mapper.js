import {Col, DatePicker, Form} from 'antd';
import {CustomInput} from '../../molecules';

const renderFieldFilter = (list) => {
  return list.map((item) => {
    if (item.type === 'date-range') {
      return (
        <Col
          span={8}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            display: 'flex',
            padding: 10,
          }}>
          <Form.Item
            name={item.fieldName}
            label={item.label}
            labelCol={{span: 6}}
            labelAlign="left"
            rules={[
              {
                required: true,
                message: 'field required',
              },
            ]}>
            <DatePicker.RangePicker
              style={{borderRadius: 5}}
              format="YYYY-MM-DD"
            />
          </Form.Item>
        </Col>
      );
    } else if (item.type === 'select') {
      return (
        <Col span={8}>
          <Form.Item
            name={item.fieldName}
            label={item.label}
            labelCol={{span: 9}}
            labelAlign="left"
            rules={[
              {
                required: true,
                message: 'field required',
              },
            ]}>
            <CustomInput
              name={item.fieldName}
              type="select"
              source={item.dataSource}
            />
          </Form.Item>
        </Col>
      );
    }
  });
};

export {renderFieldFilter};
