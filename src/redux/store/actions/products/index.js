import {productsTypes, memberTypes} from '../../action-types';
import { dismissLoading, showLoading } from '../loading';
import Services from '../../../../services';

const {SET_PRODUCTS_DETAIL, SET_PRODUCTS_LIST} = productsTypes;
const {SET_MEMBER_LIST} = memberTypes;

const setProductList = (payload) => ({
  type: SET_PRODUCTS_LIST,
  payload,
});

const setMemberList = (payload) => ({
  type: SET_MEMBER_LIST,
  payload
});

const getMemberList = (request, showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Get('/v1/user').then(result => {
      const list = result?.data?.values.map(x => {
        return {
          name: x.name,
          role: x.role,
          email: x.username
        }
      });

      dispatch(setMemberList(list));
      dismissLoading && dispatch(dismissLoading());
      resolve();
    }).catch(err => {
      window.alert(err?.response?.data?.message);
      dismissLoading && dispatch(dismissLoading());
    }).finally(() => { 
      dismissLoading && dispatch(dismissLoading());

    });
  });
};

const getProductList = (request, showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      dispatch(
        setProductList([
          {id: 1, name: 'Product 1', description: 'Product 1 Description'},
          {id: 2, name: 'Product 2', description: 'Product 2 Description'},
        ]),
      );
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const createProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const deleteProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const updateProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const setDetailProducts = (id, list, role) => (dispatch) => {
  return new Promise((resolve, reject) => {
    if (id && list) {
      let result = list.filter((item) => item.id === id)[0];
      if (result) {
        resolve(result);
        dispatch({
          type: SET_PRODUCTS_DETAIL,
          payload: result,
        });
      } else {
        reject(result);
      }
    } else {
      dispatch({
        type: SET_PRODUCTS_DETAIL,
        payload: {},
      });
    }
  });
};
export {
  getProductList,
  createProducts,
  updateProducts,
  deleteProducts,
  setDetailProducts,
  getMemberList
};
