import {
  DownOutlined,
  LogoutOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import {Col, Divider, Dropdown, Layout, Menu, Row, Space} from 'antd';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {changeLanguage} from '../../../redux/store/actions/language';
import {colors, LANGUAGE, history} from '../../../utilities';

const LayoutHeader = ({
  collapsed,
  setCollapsed,
  store,
  ProfilePicture,
  authData,
  balance,
}) => {
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation('header');
  const {language} = useSelector((state) => state.language);
  return (
    <Layout.Header
      style={{
        background: '#F0F2F5',
        padding: '1vh',
        paddingLeft: '2vh',
        paddingRight: '3vh',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      {collapsed ? (
        <MenuUnfoldOutlined
          onClick={() => setCollapsed(false)}
          style={{fontSize: 25}}
        />
      ) : (
        <MenuFoldOutlined
          onClick={() => setCollapsed(true)}
          style={{fontSize: 25}}
        />
      )}
      <Space>
        <div>
          <p
            style={{
              fontSize: 14,
              color: colors.dark,
              fontWeight: 700,
              margin: '0 0',
            }}>
            Selamat datang, {authData.name}!
          </p>
        </div>
        <Divider
          type="vertical"
          style={{borderLeft: '1px solid #292b2c', height: '2em'}}
        />
        <Dropdown
          arrow
          placement="bottomRight"
          trigger={['click']}
          overlay={
            <Menu>
              <Menu.Item key={0}>
                <Row>
                  <Col span={4}>
                    <SettingOutlined />
                  </Col>
                  <Col span={20}>
                    <div>{t('setting')}</div>
                  </Col>
                </Row>
              </Menu.Item>
              <Menu.Divider />
              <Menu.Item
                danger
                key={2}
                onClick={() => {
                  store.dispatch({type: 'USER_LOGGED_OUT'});
                  history.push('/login');
                }}>
                <Row>
                  <Col span={4}>
                    <LogoutOutlined />
                  </Col>
                  <Col span={20}>
                    <div>{t('logout')}</div>
                  </Col>
                </Row>
              </Menu.Item>
            </Menu>
          }>
          <Space align="center" style={{cursor: 'pointer'}}>
            <div style={{lineHeight: 'normal'}}>
              <p
                style={{
                  fontSize: 14,
                  color: colors.dark,
                  fontWeight: 700,
                  margin: '0 0',
                }}>
                {authData?.username}
              </p>
            </div>
            <img
              src={ProfilePicture}
              style={{width: 40, height: 40}}
              alt="profile_picture"
            />
            <DownOutlined />
          </Space>
        </Dropdown>
      </Space>
    </Layout.Header>
  );
};

export default LayoutHeader;
