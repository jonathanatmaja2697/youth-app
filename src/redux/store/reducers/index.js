import authReducer from './authentication';
import layoutReducer from './layout';
import loadingReducer from './loading';
import languageReducer from './language';
import productsReducer from './products';
import tableReducer from './table';
import membersReducer from './members';
import rolesReducer from './roles';

export {
  authReducer,
  layoutReducer,
  loadingReducer,
  languageReducer,
  productsReducer,
  tableReducer,
  membersReducer,
  rolesReducer
};
