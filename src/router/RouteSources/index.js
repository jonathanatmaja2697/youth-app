import {LayoutTemplate} from '../../components';
import {
  DashboardPage,
  LoginPage,
  PageNotFound,
  MembersFormPage,
  MemberPage,
} from '../../pages';

export const routeSources = [
  {component: LoginPage, path: '/', exact: true, key: 'LOGIN'},
  {component: LoginPage, path: '/login', exact: true, key: 'LOGIN'},
  {
    component: PageNotFound,
    path: '/404-not-found',
    exact: true,
    key: 'PAGENOTFOUND',
    title: '404 Page Not Found',
  },
  {
    layout: LayoutTemplate,
    path: '',
    child: [
      {
        component: DashboardPage,
        path: '/dashboard',
        exact: true,
        private: true,
        key: 'DASHBOARD',
      },
      {
        component: MemberPage,
        path: '/member',
        exact: true,
        private: true,
        key: 'MEMBER',
      },
      {
        component: MembersFormPage,
        path: '/member/add',
        exact: true,
        private: true,
        key: 'MEMBER',
      },
      {
        component: MembersFormPage,
        path: '/member/detail',
        exact: true,
        private: true,
        key: 'MEMBER',
      },
      {
        component: MembersFormPage,
        path: '/member/edit',
        exact: true,
        private: true,
        key: 'MEMBER',
      },
    ],
  },
];
