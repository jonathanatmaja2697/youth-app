const SET_PRODUCTS_LIST = '@products/set-products-list';
const SET_PRODUCTS_DETAIL = '@products/set-products-detail';

export {SET_PRODUCTS_LIST, SET_PRODUCTS_DETAIL};