import {BarChartOutlined, DatabaseOutlined} from '@ant-design/icons';
import {EnglishFlag, IndonesianFlag} from '../../assets';

const SIDER_MENU_LIST = [
  {
    menu: 'Dashboard',
    key: 'DASHBOARD',
    path: '/dashboard',
    role: 'all',
    icon: <BarChartOutlined />,
  },
  {
    menu: 'Members',
    key: 'MEMBER',
    path: '/member',
    role: 'all',
    icon: <DatabaseOutlined />,
  },
];

const LANGUAGE = [
  {label: 'Indonesia', value: 'id', icon: IndonesianFlag},
  {label: 'English', value: 'en', icon: EnglishFlag},
];

export {SIDER_MENU_LIST, LANGUAGE};
