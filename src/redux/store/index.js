import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import {combineReducers, createStore, applyMiddleware} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {
  authReducer,
  layoutReducer,
  loadingReducer,
  languageReducer,
  productsReducer,
  tableReducer,
  membersReducer,
  rolesReducer
} from './reducers';

const appReducer = combineReducers({
  auth: authReducer,
  layout: layoutReducer,
  loading: loadingReducer,
  language: languageReducer,
  products: productsReducer,
  table: tableReducer,
  members: membersReducer,
  roles: rolesReducer
});

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === 'USER_LOGGED_OUT') {
    state = undefined;
  }

  return appReducer(state, action);
};

export const configureStore = () => {
  const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['auth', 'language', 'table'],
  };

  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, applyMiddleware(thunk));
  const persistor = persistStore(store);

  return {
    store,
    persistor,
  };
};
