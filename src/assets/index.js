import LoginIcon from './youth.png';
import SiderIcon from './sider-icon.png';
import ProfilePicture from './profile.png';
import IndonesianFlag from './flag-indo-rectangle.png';
import EnglishFlag from './flag-uk-rectangle.png';
import LoginBackground from './login-background.png';

export {
  LoginIcon,
  SiderIcon,
  ProfilePicture,
  IndonesianFlag,
  EnglishFlag,
  LoginBackground,
};
