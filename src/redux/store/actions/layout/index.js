import {layoutActionTypes} from '../../action-types';

const {SET_SELECTED_MENU} = layoutActionTypes;

const setSelectedMenu = (payload) => ({
  type: SET_SELECTED_MENU,
  payload,
});

export {setSelectedMenu};
