import {rolesTypes} from '../../action-types';
import Services from '../../../../services';

const {GET_ROLES_LIST} = rolesTypes;

const setRolesList = (payload) => ({
  type: GET_ROLES_LIST,
  payload,
});

const getRolesList = (showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Get('/v1/role')
      .then((result) => {
        const list = result?.data?.values.map((x) => {
          return {
            value: x.name,
            label: x.name
          };
        });

        dispatch(setRolesList(list));
        dismissLoading && dispatch(dismissLoading());
        resolve();
      })
      .catch((err) => {
        window.alert(err?.response?.data?.message);
        dismissLoading && dispatch(dismissLoading());
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {getRolesList};
