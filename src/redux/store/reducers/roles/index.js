import {rolesTypes} from '../../action-types';

const {GET_ROLES_LIST} = rolesTypes;

const initialState = {
  roleList: []
};

const reducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_ROLES_LIST:
      return {...state, roleList: payload};
    default:
      return state;
  }
};

export default reducer;
