import {DeleteFilled, EditFilled, EyeOutlined} from '@ant-design/icons';
import {Col, message, Popconfirm, Row} from 'antd';
import React, {useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {Button, Container, CustomTable, PageTitle} from '../../components';
import {
  loadingActions,
  productsActions,
  memberActions,
} from '../../redux/store/actions';
import {history} from '../../utilities';

const {showLoading, dismissLoading, showLoadingTable, dismissLoadingTable} =
  loadingActions;
const {getProductList, deleteProducts} = productsActions;
const {getMemberList} = memberActions;

const ProductPage = () => {
  const dispatch = useDispatch();
  const {memberList} = useSelector((state) => state.members);
  const {t} = useTranslation('membersPage');
  useEffect(() => {
    dispatch(getMemberList(showLoading, dismissLoading));
  }, []);
  const onClickDelete = (req) => {
    dispatch(deleteProducts(req, showLoading, null)).then((res) => {
      message.success(t('deleteSuccess', {name: req.name}));
      dispatch(getProductList(null, null, dismissLoading));
    });
  };
  const createColumns = () => {
    return [
      {
        title: t('name'),
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.length - b.name.length,
      },
      {
        title: t('role'),
        dataIndex: 'role',
        key: 'role',
        sorter: (a, b) => a.role.length - b.role.length,
      },
      // {
      //   title: t('action'),
      //   dataIndex: 'action',
      //   key: 'action',
      //   width: '35%',
      //   render: (index, item) => {
      //     return (
      //       <Row>
      //         <Col xs={24} md={24} lg={24} xl={8}>
      //           <Button
      //             icon={<EyeOutlined />}
      //             text={t('buttonDetail')}
      //             type="light"
      //             rounded
      //             size="small"
      //             onClick={() => {
      //               dispatch(setDetailProducts(item.id, listProducts)).then(
      //                 () => {
      //                   history.push({
      //                     pathname: '/product/detail',
      //                     state: {type: 'DETAIL', key: 'PRODUCT'},
      //                   });
      //                 },
      //               );
      //             }}
      //           />
      //         </Col>
      //         <Col xs={24} md={24} lg={24} xl={8}>
      //           <Button
      //             icon={<EditFilled />}
      //             text={t('buttonEdit')}
      //             type="light"
      //             rounded
      //             size="small"
      //             onClick={() => {
      //               dispatch(setDetailProducts(item.id, listProducts)).then(
      //                 () => {
      //                   history.push({
      //                     pathname: '/product/edit',
      //                     state: {type: 'EDIT', key: 'PRODUCT'},
      //                   });
      //                 },
      //               );
      //             }}
      //           />
      //         </Col>
      //         <Col xs={24} md={24} lg={24} xl={8}>
      //           <Popconfirm
      //             title={t('deleteConfirm', {name: item.name})}
      //             okText={t('yes')}
      //             onConfirm={() => onClickDelete(item)}
      //             cancelText={t('no')}>
      //             <Button
      //               icon={<DeleteFilled />}
      //               text={t('buttonDelete')}
      //               type="light"
      //               rounded
      //               size="small"
      //             />
      //           </Popconfirm>
      //         </Col>
      //       </Row>
      //     );
      //   },
      // },
    ];
  };
  return (
    <div>
      <PageTitle
        showButtonAdd
        title={t('title')}
        buttonAddText={t('buttonAdd')}
        buttonAddPages={{
          page: '/member/add',
          params: {type: 'ADD', key: 'MEMBER'},
        }}
      />
      <Container>
        <CustomTable
          placeholderSearch={t('placeholderSearch')}
          filterBy={['name', 'role']}
          createColumns={createColumns}
          dataSource={memberList}
        />
      </Container>
    </div>
  );
};

export default ProductPage;
