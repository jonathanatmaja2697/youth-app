import {authActionTypes} from '../../action-types';
import Services from '../../../../services';

const {SET_AUTH_DATA} = authActionTypes;

const setAuthData = (payload) => ({
  type: SET_AUTH_DATA,
  payload,
});

const login = (request, showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      Services.Post('/v1/user/login',request).then(result => {
        dismissLoading && dispatch(dismissLoading());
        dispatch(setAuthData({
          name: result?.data?.values?.name,
          role: result?.data?.values?.role,
          id: result?.data?.values?.id,
        }));
        resolve();
      }).catch(err => {
        dismissLoading && dispatch(dismissLoading());
        window.alert(err?.response?.data?.message);
        reject(err);
      }).finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });

    }, 1000);
    // setTimeout(() => {
    //   dismissLoading && dispatch(dismissLoading());
    //   dispatch(
    //     setAuthData({
    //       username: 'admin.iki@email.com',
    //       roles: 'admin',
    //       token: 'xxx',
    //     }),
    //   );
    //   resolve();
    // }, 1000);
  });
};

export {setAuthData, login};
