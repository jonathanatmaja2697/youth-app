import {productsTypes} from '../../action-types';

const {SET_PRODUCTS_LIST, SET_PRODUCTS_DETAIL} = productsTypes;

const initialState = {
  listProducts: [],
  detailProducts: {},
};

const reducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_PRODUCTS_LIST:
      return {...state, listProducts: payload};
    case SET_PRODUCTS_DETAIL:
      return {...state, detailProducts: payload};
    default:
      return state;
  }
};

export default reducer;
