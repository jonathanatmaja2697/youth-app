# React JS Boilerplate

This repository contains a boilerplate script for React JS project. You can use this boilerplate to simplify React JS project initialization.

This boilerplate includes:

- **React Hooks** - Hooks are functions that let you “hook into” React state and lifecycle features from function components
- **React Router Dom** - Routing and navigation for your React JS apps
- **Redux** - Redux is a state management tool
- **Redux Persist** - Persist and rehydrate a redux store.
- **Redux Thunk** - Middleware for Redux Store
- **i18next** - Provide I18n translations to your React JS Projects
- **Ant Design** - Ant Design is a React UI library that contains a set of high quality components and demos for building rich, interactive user interfaces.

## Installation

To create new React JS project using this boilerplate :

- Make sure that you have Node.js latest version and yarn latest version installed.

> If you dont have yarn, please run **npm install -g yarn** to install it globally or simply go to [**https://yarnpkg.com**](https://yarnpkg.com) to read the documentation

- Clone this repository using `git clone --depth=1 https://gitlab.com/Fatanaminullah/react-dashboard-boilerplate.git <YOUR_PROJECT_NAME>`
- Run `cd <YOUR_PROJECT_NAME>` to move to your project directory
- Run `yarn run setup` in order to clean the git repo, initializing a new git repository and installing dependencies.
- Run `yarn start` to starts the development server and makes your application accessible at localhost:8080

## Directory Layout

Our Project Pattern are inspired by **Atomic Design Pattern**. Atomic design, developed by Brad Frost and Dave Olsen, is a methodology for crafting design systems with five fundamental building blocks, which, when combined, promote consistency, modularity, and scalability.

The name Atomic Design comes from the idea of separating the components in atoms, molecules, organisms, templates and pages. This project is a lil bit different from the atomic design pattern, we use a atoms > molecules > organisms > screens

**_Atoms:_**

Atoms are the smallest possible components, such as buttons, titles, inputs, etc.

**_Molecules:_**

Molecules are combination of atoms component such as combining a button, input and form label to build functionality.

**_Organisms:_**

Organisms are the combination of molecules component that work together or even with atoms component. At Organisms components begin to have the final shape but they are still ensured to be independent and reusable enough to be reusable in any content

**_Templates_**

Templates are complex component that only used in a specific pages or component

**_Pages:_**

Pages are the navigate parts of the application and it’s where the components are distributed in one specific template. The components get real content and they’re connected with the whole application.

The Folder Structure will look like this:

![Folder Structure](/folder_structure.png)

## Contributing

Everyone can contribute on this boilerplate.
