import {memberTypes} from '../../action-types';

const {SET_MEMBER_LIST} = memberTypes;

const initialState = {
  memberList: []
};

const reducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_MEMBER_LIST:
      return {...state, memberList: payload};
    default:
      return state;
  }
};

export default reducer;
