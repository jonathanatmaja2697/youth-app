import {memberTypes} from '../../action-types';
import Services from '../../../../services';

const {SET_MEMBER_LIST} = memberTypes;

const setMemberList = (payload) => ({
  type: SET_MEMBER_LIST,
  payload,
});

const getMemberList = (showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Get('/v1/user')
      .then((result) => {
        const list = result?.data?.values.map((x) => {
          return {
            name: x.name,
            role: x.role,
            email: x.username,
          };
        });

        dispatch(setMemberList(list));
        dismissLoading && dispatch(dismissLoading());
        resolve();
      })
      .catch((err) => {
        window.alert(err?.response?.data?.message);
        dismissLoading && dispatch(dismissLoading());
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {getMemberList};
